﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Project;
using ViewModel.Project;

namespace Repo.Project
{
    public static class UserRepo
    {
        public static List<VMUser> getAllData() //tipe data list<barang> method getAllData
        {
            List<VMUser> listData = new List<VMUser>();
            using (var db = new OnlineTestEntities())
            {
                listData = (from a in db.Users
                            where a.Is_Delete == false
                            select new VMUser
                            {
                                account_id = a.AccountId,
                                name = a.Name,
                                is_delete = a.Is_Delete,
                                created_by = a.CreatedBy,
                                created_at = a.CreatedAt,
                                updated_by = a.CreatedBy,
                                updated_at = a.CreatedAt
                            }).ToList();
            }
            return listData;
        }

        public static bool saveData(VMUser u)
        {
            try
            {
                using (var db = new OnlineTestEntities())
                {
                    User a = new User();
                    a.Name = u.name;
                    a.CreatedBy = "1";
                    a.CreatedAt = System.DateTime.Now;
                    a.Is_Delete = false;
                    db.Users.Add(a);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public static User getDataByKode(int ID)
        {
            User data = new User();
            using (OnlineTestEntities db = new OnlineTestEntities())
            {
                data = db.Users.Where(a => a.AccountId == ID).FirstOrDefault(); //lamda query ada juga linq query
            }
            return data;
        }

        public static bool UpdateData(VMUser u)
        {
            bool result = true;
            try
            {
                using (var db = new OnlineTestEntities())
                {
                    User a = db.Users.Where(b => b.AccountId == u.account_id).FirstOrDefault();
                    if (a!=null)
                    {
                        a.Name = u.name;
                        a.UpdatedBy = u.updated_by;
                        a.UpdatedAt = u.updated_at;
                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
                throw;
            }
            return result;
        }

        public static bool DeleteData(int ID)
        {
            try
            {
                using (OnlineTestEntities db = new OnlineTestEntities())
                {
                    //fungsi dibawah untuk mencari data
                    User data = getDataByKode(ID);
                    data.Is_Delete = true;
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }
    }
}
