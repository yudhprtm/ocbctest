﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Project;
using Repo.Project;
using ViewModel.Project;

namespace OCBCTest.Controllers
{
    public class UserController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            return PartialView("_List", UserRepo.getAllData());
        }

        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult Create(VMUser u)
        {
            if (ModelState.IsValid)
            {
                if (UserRepo.saveData(u))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int ID)
        {
            User dt = UserRepo.getDataByKode(ID);
            return PartialView("_Edit", dt);
        }

        [HttpPost]
        public ActionResult Edit(VMUser u)
        {
            if (UserRepo.UpdateData(u))
            {
                return RedirectToAction("index");
            }
            return RedirectToAction("index");
        }

        public ActionResult Delete(int ID)
        {
            if (UserRepo.DeleteData(ID))
            {
                return RedirectToAction("index");
            }
            return RedirectToAction("index");


        }
    }
}