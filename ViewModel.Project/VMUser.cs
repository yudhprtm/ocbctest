﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.Project
{
    public class VMUser
    {
        public int account_id { get; set; }
        public string name { get; set; }
        public bool is_delete { get; set; }
        public string created_by { get; set; }
        public System.DateTime created_at { get; set; }
        public string updated_by { get; set; }
        public System.DateTime updated_at { get; set; }
    }
}
